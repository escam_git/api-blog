@extends('layouts.layout')

@section('title', 'Home')

@section('content')
    <div class="img-frame header-bg">
        <img src="{{ asset('images/header-bg.jpg') }}" alt="" class="img-fluid d-block mx-auto">
    </div>

    <section>
        <div class="container">
            <h1 class="text-center mt-0 mb-15">{{ _('Latest Articles') }}</h1>
            <p class="text-center big-text">{{ _('Our most recent posts') }}</p>
        </div>
    </section>

    <news-list></news-list>
@endsection
