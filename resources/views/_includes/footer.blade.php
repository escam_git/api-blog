<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
               <small>This is the footer colophon. Tanks for all! :D</small>
            </div>

            <div class="col">
                @include('_includes._partials.social')
            </div>
        </div>
    </div>
</footer>
