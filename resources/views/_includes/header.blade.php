<header>
    <div class="container">
        <div id="logo">
            <a href="/" title=""><h1 class="my-0">My Blog</h1></a>
        </div>
    </div>
    <div class="mainMenu">
        <div class="container">
            @include('_includes._partials.menu')
        </div>
    </div>

    <div class="menuBurger" onclick="window.utils.openMenu();"><span class="menuBurger__icon"></span></div>
</header>
