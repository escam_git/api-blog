<!doctype html>
<html lang="es_ES">
	<head>
		<title>Template | @yield('title')</title>
        <meta name="description" content="@yield('description')" />

		<link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" media="screen" />
		@stack('styles')
	</head>
	<body>
		<div id="app">
			@include('_includes.header')

			<div class="generalContent">
				@yield('content')
				@include('_includes.footer')
			</div>
		</div>

		<script src="{{ asset('js/app.js') }}" charset="utf-8"></script>
		@stack('scripts')
	</body>
</html>
